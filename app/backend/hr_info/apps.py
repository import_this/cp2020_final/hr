from django.apps import AppConfig


class HrInfoConfig(AppConfig):
    name = 'hr_info'
