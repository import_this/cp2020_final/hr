from rest_framework import serializers

from hr_info.models import Vacancy, Resume, SpecializationPossibleTraits, Skills, Specialization


class VacancySerializer(serializers.ModelSerializer):

    class Meta:
        model = Vacancy
        fields = ['id', 'name', 'specialization', 'what_to_do', 'key_requirements', 'conditions', 'years_of_experience',
                  'key_skills']


class ResumeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Resume
        fields = ['id', 'first_name', 'last_name', 'patronymic', 'title', 'description', 'years_of_experience',
                  'specialization']


class SpecializationPossibleTraitsSerializer(serializers.ModelSerializer):

    class Meta:
        model = SpecializationPossibleTraits
        fields = ['id', 'name', 'text', 'specialization']


class SkillsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Skills
        fields = ['id', 'name']


class SpecializationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Specialization
        fields = ['id', 'name']
