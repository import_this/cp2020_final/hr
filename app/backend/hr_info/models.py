from django.contrib.gis.db import models


class Specialization(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class SpecializationPossibleTraits(models.Model):
    name = models.CharField(max_length=256)
    specialization = models.ForeignKey(Specialization, models.CASCADE, related_name='traits')
    text = models.TextField()

    def __str__(self):
        return self.name


class Skills(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class Vacancy(models.Model):
    name = models.CharField(max_length=256)
    specialization = models.ForeignKey(Specialization, models.CASCADE, related_name='specs')
    what_to_do = models.TextField()
    key_requirements = models.TextField()
    conditions = models.TextField()
    years_of_experience = models.IntegerField()
    key_skills = models.ManyToManyField(Skills, related_name='skills')


class Resume(models.Model):
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    patronymic = models.CharField(max_length=256, null=True, blank=True)
    title = models.CharField(max_length=256)
    description = models.TextField()
    years_of_experience = models.IntegerField()
    key_skills = models.ManyToManyField(Skills, related_name='resume')
    specialization = models.ForeignKey(Specialization, models.CASCADE, related_name='resume_spec', default=None)
    photo = models.ImageField(null=True, blank=True)
