from django.urls import include, path
from rest_framework import routers

from hr_info import views
from hr_info.views import SpecializationAutocomplete

router = routers.DefaultRouter()
router.register(r'vacancy', views.VacancyViewSet)
router.register(r'traits', views.SpecializationPossibleTraitsViewSet)
router.register(r'specialization', views.SpecializationViewSet)
router.register(r'skills', views.SkillsViewSet)
router.register(r'resume', views.ResumeViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('specialization-autocomplete', SpecializationAutocomplete.as_view())
]
