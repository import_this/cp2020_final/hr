from PIL import Image
from dal import autocomplete
from django.http import JsonResponse
from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response

from hr_info.models import Vacancy, Resume, Specialization, SpecializationPossibleTraits, Skills
from hr_info.ranking import rank_resumes_by_vacancy
from hr_info.serializers import VacancySerializer, ResumeSerializer, SpecializationSerializer, SkillsSerializer, \
    SpecializationPossibleTraitsSerializer


class VacancyViewSet(viewsets.ModelViewSet):
    queryset = Vacancy.objects.all()
    serializer_class = VacancySerializer
    permission_classes = [permissions.AllowAny]

    @action(detail=True)
    def rank_resumes(self, request, pk=None):
        vacancy = self.get_object()
        resumes = list(Resume.objects.filter(specialization=vacancy.specialization))
        ranked_indexes = rank_resumes_by_vacancy(vacancy.what_to_do, [x.description for x in resumes])
        ranked_resumes = [resumes[i] for i in ranked_indexes]
        resumes_json = ResumeSerializer(ranked_resumes, many=True)
        return Response(resumes_json.data)


class SpecializationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Specialization.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class SpecializationPossibleTraitsViewSet(viewsets.ModelViewSet):
    queryset = SpecializationPossibleTraits.objects.all()
    serializer_class = SpecializationPossibleTraitsSerializer
    permission_classes = [permissions.AllowAny]


class SpecializationViewSet(viewsets.ModelViewSet):
    queryset = Specialization.objects.all()
    serializer_class = SpecializationSerializer
    permission_classes = [permissions.AllowAny]


class SkillsViewSet(viewsets.ModelViewSet):
    queryset = Skills.objects.all()
    serializer_class = SkillsSerializer
    permission_classes = [permissions.AllowAny]


class ResumeViewSet(viewsets.ModelViewSet):
    queryset = Resume.objects.all()
    serializer_class = ResumeSerializer
    permission_classes = [permissions.AllowAny]

    @action(detail=True)
    def get_big_five(self, request, pk=None):
        from hr_info.determinate import get_big_five

        resume = self.get_object()
        if not resume.photo:
            return Response(status=418)

        resume.photo.open()
        image = Image.open(resume.photo)
        extroversion, accommodation, orderliness, emotional, intellect, big5 = get_big_five(image)
        return JsonResponse({
            'extroversion': extroversion,
            'accommodation': accommodation,
            'orderliness': orderliness,
            'emotional': emotional,
            'intellect': intellect,
            'big5': big5
        })
