from django.contrib import admin

from hr_info.models import Specialization, SpecializationPossibleTraits, Skills, Vacancy, Resume


@admin.register(Specialization)
class SpecializationAdmin(admin.ModelAdmin):
    pass


@admin.register(SpecializationPossibleTraits)
class SpecializationPossibleTraitsAdmin(admin.ModelAdmin):
    pass


@admin.register(Skills)
class SkillsAdmin(admin.ModelAdmin):
    pass


@admin.register(Vacancy)
class VacancyAdmin(admin.ModelAdmin):
    pass


@admin.register(Resume)
class ResumeAdmin(admin.ModelAdmin):
    pass
