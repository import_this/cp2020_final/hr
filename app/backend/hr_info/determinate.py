from django.core.files.storage import default_storage
from skimage.transform import resize
import torch
import torch.nn as nn
import torch.nn.functional as F


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.conv3 = nn.Conv2d(16, 32, 5)
        self.conv4 = nn.Conv2d(32, 64, 5)
        self.fc1 = nn.Linear(64 * 10 * 6, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 5)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = self.pool(F.relu(self.conv3(x)))
        x = self.pool(F.relu(self.conv4(x)))
        x = x.view(-1, 64 * 10 * 6)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


model = Net()
model.load_state_dict(torch.load(default_storage.path('personality.pt'), map_location=torch.device('cpu')))
model.eval()


def predict_personality(image):
    image = resize(image, (228, 170))
    image = image.reshape(1, 1, 228, 170).astype('float32')
    return model.forward(torch.Tensor(image)).tolist()[0]


def decode_big5(itellect, orderliness, extroversion, accommodation, emotional):
    if itellect > 0.5:
        i_res = "i"
    else:
        i_res = "n"

    if orderliness > 0.5:
        o_res = "o"
    else:
        o_res = "u"

    if extroversion > 0.5:
        ex_res = "s"
    else:
        ex_res = "r"

    if accommodation > 0.5:
        a_res = "a"
    else:
        a_res = "e"

    if emotional > 0.5:
        em_res = "l"
    else:
        em_res = "c"

    person_code = ex_res + em_res + o_res + a_res + i_res

    return person_code


def get_big_five(image_data):
    extroversion, accommodation, orderliness, emotional, intellect = predict_personality(image_data)
    return extroversion, accommodation, orderliness, emotional, intellect, decode_big5(intellect, orderliness,
                                                                                       extroversion, accommodation,
                                                                                       emotional)
