from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel


def rank_resumes_by_vacancy(vacancy_text, resume_texts_list):
    texts = [vacancy_text]
    texts.extend(resume_texts_list)

    tfidf = TfidfVectorizer().fit_transform(texts)
    return linear_kernel(tfidf[0:1], tfidf[1:]).flatten().argsort()[:-50:-1]
